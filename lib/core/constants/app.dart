const String appName = 'OpenTodo';
const String upcomingTodoDatabaseFileName = 'upcomingTodo';
const String doneTodoDatabaseFileName = 'doneTodo';
const String settingsDatabaseFileName = 'settings';
const String secureDatabaseFileName = 'secure';
const double settingsTileWidgetWidth = 150;
const String cloudProviderUrl = 'https://apis.hafijul.com/todo-api';
