import 'package:flutter_local_notifications/flutter_local_notifications.dart';

final DarwinInitializationSettings iosInitializationSettings =
    DarwinInitializationSettings(
  requestAlertPermission: false,
  requestBadgePermission: false,
  requestSoundPermission: false,
  notificationCategories: <DarwinNotificationCategory>[
    DarwinNotificationCategory(
      'textCategory',
      actions: <DarwinNotificationAction>[
        DarwinNotificationAction.text(
          'text_1',
          'Action 1',
          buttonTitle: 'Send',
          placeholder: 'Placeholder',
        ),
      ],
    ),
  ],
);

const DarwinNotificationDetails iosNotificationDetails =
    DarwinNotificationDetails(
  categoryIdentifier: 'PlainCategory',
);
