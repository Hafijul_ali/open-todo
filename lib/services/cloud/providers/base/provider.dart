// Abstract class [Provider] to be implemented by
// specific backend providers like todo-api

import '../todo_api/todo.dart';

abstract class TodoProvider {
  // TODO : Improve implementation to abstract out more todo providers
  // Works for todo-api for now with `apiKey`
  Future<dynamic> login({String? username, String? password, String? apiKey});

  Future<void> logout();

  Future<dynamic> getTodo(String id);
  Future<dynamic> postTodo(Todo todo);
}
