import 'package:dio/dio.dart';

import '../../../../core/constants/app.dart';
import '../../../../init.dart';

final BaseOptions baseOptions = BaseOptions(
    baseUrl: cloudProviderUrl,
    contentType: 'application/json',
    responseType: ResponseType.json,
    followRedirects: true,
    headers: <String, String>{'Authorization': 'Basic $apiKey'});
