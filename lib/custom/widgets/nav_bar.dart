import 'package:flutter/material.dart';

import '../../core/constants/routes.dart';
import '../../core/constants/strings.dart';
import '../../init.dart';

NavigationBar navBar(int? currentIndex, void Function(int) onItemTapped) {
  return NavigationBar(
    labelBehavior: NavigationDestinationLabelBehavior.onlyShowSelected,
    destinations: navBarsItems(),
    selectedIndex: currentIndex!,
    onDestinationSelected: onItemTapped,
  );
}

List<NavigationDestination> navBarsItems() {
  NavigationDestination editOrAddTodoButton = (currentPath == editTodoPageRoute)
      ? const NavigationDestination(
          icon: Icon(
            Icons.edit_outlined,
            color: Colors.greenAccent,
          ),
          label: editTodo,
        )
      : const NavigationDestination(
          icon: Icon(
            Icons.add_outlined,
            color: Colors.greenAccent,
          ),
          label: addTodo,
        );

  return <NavigationDestination>[
    const NavigationDestination(
      icon: Icon(
        Icons.upcoming_outlined,
        color: Colors.redAccent,
      ),
      label: upcomingTodo,
    ),
    editOrAddTodoButton,
    const NavigationDestination(
      icon: Icon(
        Icons.done_all_outlined,
        color: Colors.blueAccent,
      ),
      label: doneTodo,
    ),
  ];
}
