import 'package:flutter/material.dart';

import '../../custom/components/list_builder.dart';
import '../../init.dart';

class UpcomingTodo extends StatelessWidget {
  const UpcomingTodo({super.key});

  @override
  Widget build(BuildContext context) {
    currentPath = '/UpcomingTodoPage';
    return listBuilder(context, upcomingTodoDatabase);
  }
}
