import 'package:flutter/material.dart';

import '../../../init.dart';
import '../../core/constants/settings.dart';
import '../../custom/widgets/app_bar.dart';
import 'app_theme/app_theme.dart';
import 'cloud_auth/cloud_auth.dart';
import 'export_database/export_database.dart';
import 'font_size/font_size.dart';
import 'import_database/import_database.dart';
import 'landing_page/landing_page.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  SwitchListTile _useMaterial3(BuildContext context) {
    return SwitchListTile(
      title: const Text('Use Material 3'),
      value: useMaterial3,
      onChanged: (bool value) {
        setState(() {
          settingsDatabase!.put(material3SettingsKey, value);
          useMaterial3 = value;
        });
      },
      secondary: const Icon(Icons.design_services_outlined),
    );
  }

  List<Widget> _widgetsTiles(BuildContext context) {
    return <Widget>[
      appTheme(context),
      const SizedBox(height: 16),
      _useMaterial3(context),
      const SizedBox(height: 16),
      exportDatabase(context),
      const SizedBox(height: 16),
      importDatabase(context),
      const SizedBox(height: 16),
      landingPage(context),
      const SizedBox(height: 16),
      changeFontSize(context),
      const SizedBox(height: 16),
      cloudAuthentication(context),
      const SizedBox(height: 16),
    ];
  }

  Widget settingsPage(BuildContext context) {
    return Scaffold(
      appBar: appBar(context),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: ListView(children: _widgetsTiles(context)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    currentPath = '/SettingsPage';
    return settingsPage(context);
  }
}
